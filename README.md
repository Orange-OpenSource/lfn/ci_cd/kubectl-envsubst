# Kubectl - Envsubst

A lightweight Docker image with utilities installed for interacting with
Kubernetes and to simplify shell scripting.  This includes `kubectl` and
`envsubst` (from [Envsubst](https://github.com/a8m/envsubst)).

## Automatic updates

Base security uses
[renovatebot](https://gitlab.tech.orange/oln/nif/cd/tools/renovate/) in order to
perform automatic merge requests when upstream components are updated.

Please look at
[renovate readme](https://gitlab.tech.orange/oln/nif/cd/tools/renovate/-/blob/main/README.md)
for more explanations on the renovate configuration.
