FROM alpine:3.20.0@sha256:77726ef6b57ddf65bb551896826ec38bc3e53f75cdde31354fbffb4f25238ebd

ARG CI_COMMIT_SHORT_SHA
ARG BUILD_DATE

# renovate: datasource=repology depName=alpine_3_20/curl versioning=loose
ENV CURL_VERSION=8.7.1-r0
# renovate: datasource=github-releases depName=kubernetes/kubernetes
ENV KUBERNETES_VERSION=v1.30.1
# renovate: datasource=github-releases depName=a8m/envsubst
ENV ENVSUBST_VERSION=v1.4.2


# renovate: datasource=repology depName=alpine_3_20/libcrypto3 versioning=loose
ENV LIBCRYPTO3_VERSION=3.3.0-r2
# renovate: datasource=repology depName=alpine_3_20/libssl3 versioning=loose
ENV LIBSSL3_VERSION=3.3.0-r2

LABEL org.opencontainers.image.authors="Sylvain Desbureaux <sylvain.desbureaux@orange.com>"

SHELL ["/bin/ash", "-eox", "pipefail", "-c"]

# install build deps
RUN apk add --no-cache --virtual builddeps \
    curl="${CURL_VERSION}" \
    &&\
  apk add --no-cache \
    libcrypto3="${LIBCRYPTO3_VERSION}" \
    libssl3="${LIBSSL3_VERSION}" \
    && \
  curl -LO "https://dl.k8s.io/release/${KUBERNETES_VERSION}/bin/linux/amd64/kubectl" && \
  chmod +x kubectl &&\
  mv kubectl /usr/local/bin && \
  curl -LO "https://github.com/a8m/envsubst/releases/download/${ENVSUBST_VERSION}/envsubst-Linux-x86_64" && \
  chmod +x envsubst-Linux-x86_64 &&\
  mv envsubst-Linux-x86_64 /usr/local/bin/envsubst && \
  # clean build deps
  apk del builddeps &&\
  rm -rf /var/cache/apk/*

CMD [ "/bin/ash" ]
